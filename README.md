# Mushrooms Picking

This project is about mushrooms 🍄 picking

## Registration of the runner

```shell
gitlab-runner register \
  --non-interactive \
  --registration-token <YOUR_TOKEN> \
  --locked=false \
  --description runner-of-k33g \
  --url https://gitlab.com/ \
  --executor docker \
  --docker-image docker:stable
```